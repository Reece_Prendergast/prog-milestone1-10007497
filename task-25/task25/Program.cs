﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task25
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Type a number");

            try
            {

                var input = int.Parse(Console.ReadLine());
                Console.WriteLine($"{input} x 5 = {input / 1}");
            }
            catch (Exception r)
            {

                Console.WriteLine(r);
            }
        }
    }
}

