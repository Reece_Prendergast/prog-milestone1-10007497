﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task18
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new List<Tuple<string, int, string>>();
            name.Add(Tuple.Create("bob", 04,"april")); // "people’s name and month and birthday" no idea what this means so i did it like this.
            name.Add(Tuple.Create("james", 05, "june"));
            name.Add(Tuple.Create("luke", 06, "march"));

            foreach (var x in name)
            {
                Console.WriteLine($"my name is {x.Item1} i was born on the {x.Item2} th of {x.Item3}");

            }

        }
    }
}
