﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task22
{
    class Program
    {
        static void Main(string[] args)
        {
            var abp = new Dictionary<string, string>();
            var abpList = new List<string>();

            abp.Add("apple", "fruit");
            abp.Add("orange", "fruit");
            abp.Add("grapes", "fruit");
            abp.Add("pears", "fruit");
            abp.Add("tomatoes", "vegetable");
            abp.Add("onions", "vegetable");
            abp.Add("potatoes", "vegetable");
            abp.Add("beets", "vegetable");




            foreach (var x in abp)
            {
                Console.WriteLine($"{x.Key} & {x.Value}");
                if (x.Value == "fruit")
                {

                    abpList.Add(x.Key);


                }
            }

            Console.WriteLine($"in the list of fruits and vegetables there are {abpList.Count} ");
            Console.WriteLine($"These are the names of the fruits: {string.Join(", ", abpList)}");
        }
    }
}
