﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task30
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            int sum;
            Console.WriteLine("enter first number");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("enter second number");
            b = int.Parse(Console.ReadLine());
            sum = a + b; // add numbers
            
           Console.WriteLine("Sum is {0}", sum);
           Console.WriteLine($"the first number {a} and {b} = {sum}");

            Console.ReadLine();
        }
    }
}
