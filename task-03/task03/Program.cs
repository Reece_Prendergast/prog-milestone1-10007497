﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type k for kilometer or m for miles");

            var metric = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;

            switch (metric)
            {
                case "k":
                    Console.WriteLine("type a number you want to covert from km to miles");
                    var input = double.Parse(Console.ReadLine());
                    var answer = Math.Round(input * km2mile, 2);
                    Console.WriteLine($"{input} KM is {answer} Miles");
                    break;

                case "m":
                    Console.WriteLine("type a number you want to covert from km to miles");
                    var getMile = double.Parse(Console.ReadLine());
                    var roundingMile = System.Math.Round(getMile * mile2km, 2);
                    Console.WriteLine($"{getMile} KM is {roundingMile} Miles");
                    break;

                default:
                    Console.WriteLine("please type in k for kilometer or m for miles - please try again");
                    break;


            }

        }
    }
}